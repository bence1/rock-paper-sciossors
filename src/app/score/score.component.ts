import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-score',
  templateUrl: './score.component.html',
  styleUrls: ['./score.component.scss']
})
export class ScoreComponent implements OnInit {
  score = Number(localStorage.getItem('score'));

  constructor(private dataService: DataService) {
    this.dataService.currentScore.subscribe(
      (currentScore: number) => (this.score = currentScore)
    );
  }

  ngOnInit(): void {}
}
