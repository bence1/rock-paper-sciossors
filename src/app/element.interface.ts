export interface Element {
  name: string;
  img: string;
  color: string;
}
