import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class DataService {
  currentScore = new EventEmitter<number>();
}
