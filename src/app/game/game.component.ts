import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { data } from '../../assets/data';
import { Element } from '../element.interface';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {
  elements: Array<any> = [];
  computerSelectedItem: Element;
  yourItem: Element;
  // Get your score from Local Storage
  score: number = Number(localStorage.getItem('score'));

  gameStatus: boolean = false;
  rulesText: boolean = false;

  result: string = '';

  constructor(private dataService: DataService) {}

  ngOnInit(): void {
    this.elements = [...data];
  }

  elementClicked(element) {
    this.yourItem = element;

    // Computer randomly select an element to a buttoncclick
    this.computerSelectedItem = this.elements[
      Math.floor(Math.random() * this.elements.length)
    ];

    this.checkResult(element);
  }

  checkResult(element) {
    switch (element.name + this.computerSelectedItem.name) {
      case 'rockscissors':
      case 'rocklizard':
      case 'paperrock':
      case 'paperspock':
      case 'scissorspaper':
      case 'scissorslizard':
      case 'lizardpaper':
      case 'lizardspock':
      case 'spockscissors':
      case 'spockrock':
        this.result = 'YOU WIN';
        this.score++;
        // Emit your score with dataService
        this.dataService.currentScore.emit(this.score);

        //save the new score to Local Storage
        const scoreString = this.score;
        localStorage.setItem('score', scoreString.toString());
        break;
      case 'rockpaper':
      case 'rockspock':
      case 'scissorsrock':
      case 'sciccorsspock':
      case 'paperscissors':
      case 'paperlizard':
      case 'lizardscissors':
      case 'lizardrock':
      case 'spockpaper':
      case 'spocklizard':
        this.result = 'YOU LOSE';
        break;
      default:
        this.result = 'DRAW';
        break;
    }
    this.gameStatus = true;
  }

  playAgain() {
    this.gameStatus = false;
  }

  openRules() {
    this.rulesText = !this.rulesText;
  }
}
