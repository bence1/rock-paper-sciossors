export const data = [
  {
    name: "rock",
    img: "/assets/images/icon-rock.svg",
    color: "#f00"
  },
  {
    name: "paper",
    img: "/assets/images/icon-paper.svg",
    color: "#0000ff"
  },
  {
    name: "scissors",
    img: "/assets/images/icon-scissors.svg",
    color: "#ffff00"
  },
  {
    name: "lizard",
    img: "/assets/images/icon-lizard.svg",
    color: "rgb(150,45,235) "
  },
  {
    name: "spock",
    img: "/assets/images/icon-spock.svg",
    color: "rgb(0,187,2015)"
  }
];
